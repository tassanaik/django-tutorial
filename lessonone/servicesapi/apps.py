from django.apps import AppConfig


class ServicesapiConfig(AppConfig):
    name = 'servicesapi'
