from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions
import base64

class CustomAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        authentications = request.headers['Authorization']
        if not authentications.startswith("Basic") :
            return None       
        (username, password) = base64.b64decode(authentications[5:].strip()).decode("utf-8").split(':',1)
        user = User(username=username)
        return ( user,  None)