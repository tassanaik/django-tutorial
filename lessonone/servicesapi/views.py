from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
import ibm_db
import sys
# Create your views here.

@api_view(['POST'])
def apiRequest(request):
    print(request.body)
    jsonRequest = json.loads( request.body )
    return Response( data={ "status" : "OK", "name" : jsonRequest["username"]})

'''
@api_view(['POST'])
def contractInfo(request):
    jsonRequest = json.loads( request.body )
    print("connect to database")

    ibm_db_conn = ibm_db.connect('DATABASE=testdb;HOSTNAME=localhost;PORT=50000;PROTOCOL=TCPIP;UID=DB2INST1;PWD=password;','','')
    results = {}

    try:
        sql = 'SELECT * FROM COLLECTIONS.CONTRACT_INFO WHERE CONTRACT_NO=?'
        stmt = ibm_db.prepare(ibm_db_conn, sql)
        ibm_db.bind_param(stmt, 1, jsonRequest["contractNo"])
        ibm_db.execute(stmt)
        dict = ibm_db.fetch_assoc(stmt)
        if dict!=False :
            results = dict
        
        # while dict!=False:
        #     results.append( dict )
        #     dict = ibm_db.fetch_assoc(stmt)
        # ibm_db.close()
    except :
        print(sys.exc_info())
    ibm_db.close(ibm_db_conn)

    return Response( data={ "name" : results })
'''